# Jaroslav Svoboda <multi.flexi@seznam.cz>, 2018. #zanata
# Jaroslav Svoboda <multi.flexi@seznam.cz>, 2019. #zanata
msgid ""
msgstr ""
"Project-Id-Version: purism-chatty\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-03-05 12:35+0100\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2019-06-13 09:43-0400\n"
"Last-Translator: Jaroslav Svoboda <multi.flexi@seznam.cz>\n"
"Language-Team: Slovak\n"
"Language: sk\n"
"X-Generator: Zanata 4.6.2\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2\n"

#: data/sm.puri.Chatty.appdata.xml.in:6 data/sm.puri.Chatty.desktop.in:3
#: src/chatty-window.c:483 src/ui/chatty-window.ui:210
msgid "Chats"
msgstr ""

#: data/sm.puri.Chatty.appdata.xml.in:7
msgid "A messaging application"
msgstr ""

#: data/sm.puri.Chatty.appdata.xml.in:9
msgid "Chats is a messaging application supporting XMPP and SMS."
msgstr ""

#: data/sm.puri.Chatty.appdata.xml.in:16
msgid "Chats message window"
msgstr ""

#: data/sm.puri.Chatty.appdata.xml.in:29
msgid "Initial development release of chatty with support for xmpp and sms."
msgstr ""

#: data/sm.puri.Chatty.desktop.in:5
msgid "sm.puri.Chatty"
msgstr "sm.puri.Chatty"

#: data/sm.puri.Chatty.desktop.in:6
msgid "SMS and XMPP chat application"
msgstr ""

#: data/sm.puri.Chatty.desktop.in:7
msgid "XMPP;SMS;chat;jabber;messaging;modem"
msgstr ""

#: data/sm.puri.Chatty.gschema.xml:7 data/sm.puri.Chatty.gschema.xml:8
msgid "Whether the application is launching the first time"
msgstr ""

#: data/sm.puri.Chatty.gschema.xml:13
msgid "Send message read receipts"
msgstr ""

#: data/sm.puri.Chatty.gschema.xml:14
msgid "Whether to send the status of the message if read"
msgstr ""

#: data/sm.puri.Chatty.gschema.xml:19
msgid "Message carbon copies"
msgstr ""

#: data/sm.puri.Chatty.gschema.xml:20 src/ui/chatty-settings-dialog.ui:151
msgid "Share chat history among devices"
msgstr ""

#: data/sm.puri.Chatty.gschema.xml:25
msgid "Enable Message Archive Management"
msgstr ""

#: data/sm.puri.Chatty.gschema.xml:26
msgid "Enable MAM archive synchronization from the server"
msgstr ""

#: data/sm.puri.Chatty.gschema.xml:31
msgid "Send typing notifications"
msgstr ""

#: data/sm.puri.Chatty.gschema.xml:32
msgid "Whether to Send typing notifications"
msgstr ""

#: data/sm.puri.Chatty.gschema.xml:37 data/sm.puri.Chatty.gschema.xml:38
msgid "Mark offline users differently"
msgstr ""

#: data/sm.puri.Chatty.gschema.xml:43 data/sm.puri.Chatty.gschema.xml:44
msgid "Mark Idle users differently"
msgstr ""

#: data/sm.puri.Chatty.gschema.xml:49 data/sm.puri.Chatty.gschema.xml:50
msgid "Indicate unknown contacts"
msgstr ""

#: data/sm.puri.Chatty.gschema.xml:55
msgid "Convert text to emoticons"
msgstr ""

#: data/sm.puri.Chatty.gschema.xml:56
msgid "Convert text matching emoticons as real emoticons"
msgstr ""

#: data/sm.puri.Chatty.gschema.xml:61
msgid "Enter key sends the message"
msgstr ""

#: data/sm.puri.Chatty.gschema.xml:62
msgid "Whether pressing Enter key sends the message"
msgstr ""

#: src/chatty-application.c:61
msgid "Show release version"
msgstr ""

#: src/chatty-application.c:62
msgid "Start in daemon mode"
msgstr ""

#: src/chatty-application.c:63
msgid "Disable all accounts"
msgstr ""

#: src/chatty-application.c:64
msgid "Enable libpurple debug messages"
msgstr ""

#: src/chatty-application.c:65
msgid "Enable verbose libpurple debug messages"
msgstr ""

#: src/chatty-buddy-list.c:804
msgid "Disconnect group chat"
msgstr ""

#: src/chatty-buddy-list.c:805
msgid "This removes chat from chats list"
msgstr ""

#: src/chatty-buddy-list.c:809
msgid "Delete chat with"
msgstr ""

#: src/chatty-buddy-list.c:810
msgid "This deletes the conversation history"
msgstr ""

#: src/chatty-buddy-list.c:821 src/chatty-purple-request.c:190
#: src/dialogs/chatty-settings-dialog.c:448
#: src/dialogs/chatty-user-info-dialog.c:76 src/ui/chatty-dialog-join-muc.ui:16
#: src/ui/chatty-dialog-muc-info.ui:56
msgid "Cancel"
msgstr ""

#: src/chatty-buddy-list.c:823
msgid "Delete"
msgstr ""

#: src/chatty-buddy-list.c:1102
msgid "Me: "
msgstr ""

#: src/chatty-connection.c:76
msgid "Login failed"
msgstr ""

#: src/chatty-connection.c:82
msgid "Please check ID and password"
msgstr ""

#: src/chatty-conversation.c:1458
msgid "Owner"
msgstr ""

#: src/chatty-conversation.c:1461
msgid "Moderator"
msgstr ""

#: src/chatty-conversation.c:1464
msgid "Member"
msgstr ""

#: src/chatty-conversation.c:2161
#, c-format
msgid "New message from %s"
msgstr ""

#: src/chatty-message-list.c:73 src/chatty-message-list.c:78
msgid "This is a IM conversation."
msgstr "Toto je IM konverzácia."

#: src/chatty-message-list.c:74 src/chatty-message-list.c:84
msgid "Your messages are not encrypted,"
msgstr "Vaše správy nie sú šifrované,"

#: src/chatty-message-list.c:75
msgid "ask your counterpart to use E2EE."
msgstr "požiadajte váš náprotivok o použití E2EE."

#: src/chatty-message-list.c:79
msgid "Your messages are secured"
msgstr "Vaše správy sú zabezpečené"

#: src/chatty-message-list.c:80
msgid "by end-to-end encryption."
msgstr "E2E šifrovaním."

#: src/chatty-message-list.c:83
msgid "This is a SMS conversation."
msgstr "Toto je SMS konverzácia."

#: src/chatty-message-list.c:85
#, fuzzy
msgid "and carrier rates may apply."
msgstr "a môže byť spoplatnené operátorom."

#: src/chatty-notify.c:80
msgid "Open Message"
msgstr ""

#: src/chatty-notify.c:85
msgid "Message Received"
msgstr ""

#: src/chatty-notify.c:91
msgid "Message Error"
msgstr ""

#: src/chatty-notify.c:97
msgid "Account Info"
msgstr ""

#: src/chatty-notify.c:103
msgid "Account Connected"
msgstr ""

#: src/chatty-notify.c:110
msgid "Open Account Settings"
msgstr ""

#: src/chatty-notify.c:113
msgid "Account Disconnected"
msgstr ""

#: src/chatty-purple-notify.c:41
msgid "Close"
msgstr ""

#: src/chatty-purple-request.c:185
msgid "Save File..."
msgstr ""

#: src/chatty-purple-request.c:186
msgid "Open File..."
msgstr ""

#: src/chatty-purple-request.c:192
msgid "Save"
msgstr ""

#: src/chatty-purple-request.c:192 src/dialogs/chatty-settings-dialog.c:447
#: src/dialogs/chatty-user-info-dialog.c:75
msgid "Open"
msgstr ""

#: src/chatty-utils.c:48
msgid "About "
msgstr ""

#: src/chatty-utils.c:49
msgid "Less than "
msgstr ""

#: src/chatty-utils.c:50
msgid " seconds"
msgstr ""

#: src/chatty-utils.c:51
msgid " minute"
msgstr ""

#: src/chatty-utils.c:52
msgid " minutes"
msgstr ""

#: src/chatty-utils.c:53
msgid " hour"
msgstr ""

#: src/chatty-utils.c:54
msgid " hours"
msgstr ""

#: src/chatty-utils.c:55
msgid " day"
msgstr ""

#: src/chatty-utils.c:56
msgid " days"
msgstr ""

#: src/chatty-utils.c:57
msgid " month"
msgstr ""

#: src/chatty-utils.c:58
msgid " months"
msgstr ""

#: src/chatty-utils.c:59
msgid " year"
msgstr ""

#: src/chatty-utils.c:60
msgid " years"
msgstr ""

#: src/chatty-utils.c:64
msgid "s"
msgstr ""

#: src/chatty-utils.c:65 src/chatty-utils.c:66
msgid "m"
msgstr ""

#: src/chatty-utils.c:67 src/chatty-utils.c:68
msgid "h"
msgstr ""

#: src/chatty-utils.c:69 src/chatty-utils.c:70
msgid "d"
msgstr ""

#: src/chatty-utils.c:71
msgid "mo"
msgstr ""

#: src/chatty-utils.c:72
msgid "mos"
msgstr ""

#: src/chatty-utils.c:73 src/chatty-utils.c:74
msgid "y"
msgstr ""

#: src/chatty-utils.c:177
msgid "Over"
msgstr ""

#: src/chatty-utils.c:181
msgid "Almost"
msgstr ""

#: src/chatty-window.c:102 src/chatty-window.c:107 src/chatty-window.c:112
msgid "Choose a contact"
msgstr ""

#: src/chatty-window.c:103
msgid ""
"Select an <b>SMS</b> or <b>Instant Message</b> contact with the <b>\"+\"</b> "
"button in the titlebar."
msgstr ""

#: src/chatty-window.c:108
msgid ""
"Select an <b>Instant Message</b> contact with the \"+\" button in the "
"titlebar."
msgstr ""

#: src/chatty-window.c:113
msgid "Start a <b>SMS</b> chat with with the \"+\" button in the titlebar."
msgstr ""

#: src/chatty-window.c:114 src/chatty-window.c:118
msgid ""
"For <b>Instant Messaging</b> add or activate an account in "
"<i>\"preferences\"</i>."
msgstr ""

#: src/chatty-window.c:117
msgid "Start chatting"
msgstr ""

#: src/chatty-window.c:485
msgid "An SMS and XMPP messaging client"
msgstr ""

#: src/chatty-window.c:492
msgid "translator-credits"
msgstr ""

#: src/chatty-window.c:627
#, c-format
msgid "Authorize %s?"
msgstr ""

#: src/chatty-window.c:631
msgid "Reject"
msgstr ""

#: src/chatty-window.c:633
msgid "Accept"
msgstr ""

#: src/chatty-window.c:638
#, c-format
msgid "Add %s to contact list"
msgstr ""

#: src/chatty-window.c:668
msgid "Contact added"
msgstr ""

#: src/chatty-window.c:671
#, c-format
msgid "User %s has added %s to the contacts"
msgstr ""

#: src/dialogs/chatty-settings-dialog.c:320
msgid "Select Protocol"
msgstr ""

#: src/dialogs/chatty-settings-dialog.c:325
msgid "Add XMPP account"
msgstr ""

#: src/dialogs/chatty-settings-dialog.c:356
msgid "connected"
msgstr ""

#: src/dialogs/chatty-settings-dialog.c:358
msgid "connecting…"
msgstr ""

#: src/dialogs/chatty-settings-dialog.c:360
msgid "disconnected"
msgstr ""

#: src/dialogs/chatty-settings-dialog.c:444
#: src/dialogs/chatty-user-info-dialog.c:72
msgid "Set Avatar"
msgstr ""

#: src/dialogs/chatty-settings-dialog.c:514
#: src/ui/chatty-settings-dialog.ui:468
msgid "Delete Account"
msgstr ""

#: src/dialogs/chatty-settings-dialog.c:517
#, c-format
msgid "Delete account %s?"
msgstr ""

#: src/dialogs/chatty-user-info-dialog.c:206
msgid "Encryption not available"
msgstr ""

#: src/dialogs/chatty-user-info-dialog.c:250
#: src/dialogs/chatty-user-info-dialog.c:256
msgid "This chat is not encrypted"
msgstr ""

#: src/dialogs/chatty-user-info-dialog.c:253
msgid "Encryption is not available"
msgstr ""

#: src/dialogs/chatty-user-info-dialog.c:260
msgid "This chat is encrypted"
msgstr ""

#: src/dialogs/chatty-user-info-dialog.c:470
msgid "Phone Number:"
msgstr ""

#: src/dialogs/chatty-muc-info-dialog.c:335
msgid "members"
msgstr ""

#: src/ui/chatty-dialog-join-muc.ui:12 src/ui/chatty-window.ui:19
msgid "New Group Chat"
msgstr ""

#: src/ui/chatty-dialog-join-muc.ui:28
msgid "Join Chat"
msgstr ""

#: src/ui/chatty-dialog-join-muc.ui:81 src/ui/chatty-dialog-new-chat.ui:236
msgid "Select chat account"
msgstr ""

#: src/ui/chatty-dialog-join-muc.ui:155
msgid "Password (optional)"
msgstr ""

#: src/ui/chatty-dialog-muc-info.ui:26
msgid "Group Details"
msgstr ""

#: src/ui/chatty-dialog-muc-info.ui:52
msgid "Invite Contact"
msgstr ""

#: src/ui/chatty-dialog-muc-info.ui:69
msgid "Invite"
msgstr ""

#: src/ui/chatty-dialog-muc-info.ui:166
msgid "Room topic"
msgstr ""

#: src/ui/chatty-dialog-muc-info.ui:225
msgid "Room settings"
msgstr ""

#: src/ui/chatty-dialog-muc-info.ui:248 src/ui/chatty-dialog-user-info.ui:210
msgid "Notifications"
msgstr ""

#: src/ui/chatty-dialog-muc-info.ui:249
msgid "Show notification badge"
msgstr ""

#: src/ui/chatty-dialog-muc-info.ui:265
msgid "Status Messages"
msgstr ""

#: src/ui/chatty-dialog-muc-info.ui:266
msgid "Show status messages in chat"
msgstr ""

#: src/ui/chatty-dialog-muc-info.ui:289
msgid "0 members"
msgstr ""

#: src/ui/chatty-dialog-muc-info.ui:385
msgid "Invite Message"
msgstr ""

#: src/ui/chatty-dialog-new-chat.ui:26
msgid "Start Chat"
msgstr ""

#: src/ui/chatty-dialog-new-chat.ui:55
msgid "New Contact"
msgstr ""

#: src/ui/chatty-dialog-new-chat.ui:83 src/ui/chatty-window.ui:112
msgid "Add Contact"
msgstr ""

#: src/ui/chatty-dialog-new-chat.ui:148
msgid "Send To:"
msgstr ""

#: src/ui/chatty-dialog-new-chat.ui:168
msgid "Send to"
msgstr ""

#: src/ui/chatty-dialog-new-chat.ui:287
msgid "Name (optional)"
msgstr ""

#: src/ui/chatty-dialog-new-chat.ui:325 src/ui/chatty-window.ui:138
msgid "Add to Contacts"
msgstr ""

#: src/ui/chatty-dialog-user-info.ui:12 src/ui/chatty-window.ui:98
msgid "Chat Details"
msgstr ""

#: src/ui/chatty-dialog-user-info.ui:97
msgid "XMPP ID"
msgstr ""

#: src/ui/chatty-dialog-user-info.ui:112 src/ui/chatty-dialog-user-info.ui:226
msgid "Encryption"
msgstr ""

#: src/ui/chatty-dialog-user-info.ui:127 src/ui/chatty-settings-dialog.ui:381
msgid "Status"
msgstr ""

#: src/ui/chatty-dialog-user-info.ui:227
msgid "Secure messaging using OMEMO"
msgstr ""

#: src/ui/chatty-dialog-user-info.ui:249
msgid "Fingerprints"
msgstr ""

#: src/ui/chatty-message-list-popover.ui:18
msgid "Copy"
msgstr ""

#: src/ui/chatty-pane-msg-view.ui:14
msgid "Unencrypted"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:12 src/ui/chatty-window.ui:57
msgid "Preferences"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:21
msgid "Back"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:41
msgid "_Add"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:57
msgid "_Save"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:91
msgid "Accounts"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:105
msgid "Add new account…"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:117
msgid "Privacy"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:122
msgid "Message Receipts"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:123
msgid "Confirm received messages"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:136
msgid "Message Archive Management"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:137
msgid "Sync conversations with chat server"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:150
msgid "Message Carbon Copies"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:164
msgid "Typing Notification"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:165
msgid "Send typing messages"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:181
msgid "Chats List"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:186
msgid "Indicate Offline Contacts"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:187
msgid "Grey out avatars from offline contacts"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:200
msgid "Indicate Idle Contacts"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:201
msgid "Blur avatars from offline contacts"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:214
msgid "Indicate Unknown Contacts"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:215
msgid "Color unknown contact ID red"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:231
msgid "Editor"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:236
msgid "Graphical Emoticons"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:237
msgid "Convert ASCII emoticons"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:250
msgid "Return = Send Message"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:251
msgid "Send message with return key"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:319
msgid "Account ID"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:352
msgid "Protocol"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:410 src/ui/chatty-settings-dialog.ui:707
msgid "Password"
msgstr "Heslo"

#: src/ui/chatty-settings-dialog.ui:485
msgid "Own Fingerprint"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:511
msgid "Other Devices"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:595
msgid "XMPP"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:609
msgid "Matrix"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:624
msgid "Telegram"
msgstr ""

#: src/ui/chatty-settings-dialog.ui:682
msgid "Provider"
msgstr ""

#: src/ui/chatty-window.ui:32
msgid "New Direct Chat"
msgstr ""

#: src/ui/chatty-window.ui:70
msgid "About Chats"
msgstr ""

#: src/ui/chatty-window.ui:163
msgid "Leave Chat"
msgstr ""

#: src/ui/chatty-window.ui:176
msgid "Delete Chat"
msgstr ""

#: src/users/chatty-contact.c:405
msgid "Mobile"
msgstr ""

#: src/users/chatty-contact.c:407
msgid "Work"
msgstr ""

#: src/users/chatty-contact.c:409
msgid "Home"
msgstr ""

#: src/users/chatty-contact.c:411
msgid "Other"
msgstr ""
